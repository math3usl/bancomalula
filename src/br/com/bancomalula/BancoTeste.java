package br.com.bancomalula;

/**
 * 
 * @author Matheus Santos
 *
 */
public class BancoTeste {

	public static void main(String[] args) {
		
		Conta.saldoBanco = 5_000.00;
		ContaCorrente cc = new ContaCorrente(1, 500, "123!", 
			new Cliente("Irineu", "123456", "654321", "irineu@vcnaosabenemeu.com", Sexo.MASCULINO));
		
		ContaPoupanca cp = new ContaPoupanca(1, 100.00, "789!", 
			new Cliente("Xablau", "456789", "987654", "xablau@email.com", Sexo.MASCULINO));
		
		System.out.println("DADOS DA CONTA");
		System.out.println("N�: " + cc.getNumero());
		System.out.println("Saldo: " + cc.getSaldo());
		System.out.println("Senha: " + cc.getSenha());
		System.out.println("Titular: " + cc.getCliente().getNome());
		System.out.println("RG: " + cc.getCliente().getRg());
		System.out.println("CPF: " + cc.getCliente().getCpf());
		System.out.println("E-mail: " + cc.getCliente().getEmail());
		System.out.println("Sexo: " + cc.getCliente().getSexo().nome);
	}
}