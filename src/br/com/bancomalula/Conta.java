package br.com.bancomalula;

/**
 * 
 * @author Matheus Santos
 *
 */
public class Conta {
	
	public static double saldoBanco;
	
	//atributos
	private int numero;	
	private double saldo;
	private String senha;
	private Cliente cliente;
	
	// CONSTRUTOR
	public Conta(int numero, double saldo, String senha, Cliente cliente) {
		this.numero = numero;
		this.saldo = saldo;
		this.senha = senha;
		this.cliente = cliente;
	}	
	
	// GETTERS & SETTERS
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}	
	
	
	// M�TODOS 
	public void exibeSaldo() {
		System.out.println(cliente.getNome() + " seu saldo � de R$ " + this.getSaldo());
	}
	
	public void saca(double valor) {
		this.saldo -= valor;
		Conta.saldoBanco -= valor;		
	}
	
	public void deposita(double valor) {
		this.saldo += valor;
		Conta.saldoBanco += valor;		
	}
	
	public void transferePara(Conta destino, double valor){
		this.saca(valor);
		destino.deposita(valor);
	}
}